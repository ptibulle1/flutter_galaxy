import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Galaxy',
      home: new Scaffold(
        appBar: new AppBar(
          title: const Text('Flutter Galaxy'),
        ),
        body: const Center(
          child: const Text('Hello World'),
        ),
      ),
    );
  }
}